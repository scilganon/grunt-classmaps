
'use strict';

module.exports = function (grunt) {
    grunt.registerTask('classmaps', "Use to make list of downloaded js-files", function() {
        var name = '',
            files = [],
            options = this.options({}),
            fileList = options.dest,
            funcToFile = options.callback;

        //parse directories
        grunt.file.recurse(options.src, function(abspath, rootdir, subdir, filename){
            name = [rootdir, subdir, filename].join("/");

            if (!filename.match((options.ignored || []).join("|"))){
                grunt.log.debug(name);

                files.push(name.replace(/public/,""));
            }
        });

        //write to file
        grunt.file.write(fileList, ["(", funcToFile, "(",JSON.stringify(files), ")", ");"].join(''));

        grunt.log.oklns([fileList, ': ', files.length, ' file(s) saved'].join(''));
    });
};